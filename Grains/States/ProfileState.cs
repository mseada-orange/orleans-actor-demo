﻿namespace Grains.States;

[Serializable]
public class ProfileState
{
    public Dictionary<string, string> Fields { get; } = new();
}
