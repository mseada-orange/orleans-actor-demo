﻿using GrainInterfaces;
using Grains.States;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;

namespace Grains;
public class DialGrain : Grain, IDialGrain
{
    private readonly IPersistentState<ProfileState> _profile;
    private readonly ILogger<DialGrain> _logger;

    public DialGrain(ILogger<DialGrain> logger, [PersistentState("profile", "dialStore")] IPersistentState<ProfileState> profile)
    {
        _logger = logger;
        _profile = profile;
    }

    public Task<string?> GetField(string fieldName)
    {
        var value = _profile.State.Fields.GetValueOrDefault(fieldName);
        return Task.FromResult(value);
    }

    public Task SetField(string fieldName, string fieldValue)
    {
        var dial = this.GetPrimaryKeyString();
        _logger.LogInformation("SetField::{fieldName}::{fieldValue}::{dial}", fieldName, fieldValue, dial);
        _profile.State.Fields.TryAdd(fieldName, fieldValue);
        return _profile.WriteStateAsync();
    }
}
