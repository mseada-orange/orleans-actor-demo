﻿using Orleans;

namespace GrainInterfaces;

public interface IDialGrain : IGrainWithStringKey
{
    Task SetField(string fieldName, string fieldValue);
    Task<string?> GetField(string fieldName);
}
