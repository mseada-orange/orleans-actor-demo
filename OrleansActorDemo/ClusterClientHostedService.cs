﻿using Orleans;
using Orleans.Configuration;

namespace API;

public class ClusterClientHostedService : IHostedService
{
    public IClusterClient Client { get; }

    public ClusterClientHostedService(ILoggerProvider loggerProvider)
    {
        var redisConnectionString = "localhost:6379";
        var builder = new ClientBuilder();
        builder.UseRedisClustering(options => options.ConnectionString = redisConnectionString);
        builder.Configure<ClusterOptions>(options =>
        {
            options.ClusterId = "dev";
            options.ServiceId = "Datalake360";
        });
        builder.ConfigureLogging(builder => builder.AddProvider(loggerProvider));
        Client = builder.Build();
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        await Client.Connect();
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        await Client.Close();

        Client.Dispose();
    }
}
