﻿namespace API.Requests
{
    public class GetFieldRequest
    {
        public string Dial { get; init; } = default!;
        public string Field { get; init; } = default!;
    }
}
