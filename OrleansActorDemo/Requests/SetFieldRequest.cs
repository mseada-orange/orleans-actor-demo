﻿namespace API.Requests
{
    public class SetFieldRequest
    {
        public string Dial { get; init; } = default!;
        public string Field { get; init; } = default!;
        public string Value { get; init; } = default!;
    }
}
