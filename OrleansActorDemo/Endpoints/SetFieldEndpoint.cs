﻿using API.Requests;
using FastEndpoints;
using GrainInterfaces;
using Orleans;

namespace API.Endpoints;

public class SetFieldEndpoint : Endpoint<SetFieldRequest, SetFieldResponse>
{
    private readonly IClusterClient _client;

    public SetFieldEndpoint(IClusterClient client)
    {
        _client = client;
    }


    public override void Configure()
    {
        Post("/api/set-field");
        AllowAnonymous();
    }

    public override async Task HandleAsync(SetFieldRequest req, CancellationToken ct)
    {
        var dialGrain = _client.GetGrain<IDialGrain>(req.Dial);
        await dialGrain.SetField(req.Field, req.Value);

        var res = new SetFieldResponse
        {
            Success = true,
        };

        await SendAsync(res, StatusCodes.Status200OK, ct);
    }
}
