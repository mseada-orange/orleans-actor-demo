﻿using API.Requests;
using FastEndpoints;
using GrainInterfaces;
using Orleans;

namespace API.Endpoints;

public class GetFieldEndpoint : Endpoint<GetFieldRequest, GetFieldResponse>
{
    private readonly IClusterClient _client;

    public GetFieldEndpoint(IClusterClient client)
    {
        _client = client;
    }


    public override void Configure()
    {
        Get("/api/get-field");
        AllowAnonymous();
    }

    public override async Task HandleAsync(GetFieldRequest req, CancellationToken ct)
    {
        var dialGrain = _client.GetGrain<IDialGrain>(req.Dial);
        var fieldValue = await dialGrain.GetField(req.Field);

        var res = new GetFieldResponse
        {
            Value = fieldValue,
        };

        await SendAsync(res, StatusCodes.Status200OK, ct);
    }
}
