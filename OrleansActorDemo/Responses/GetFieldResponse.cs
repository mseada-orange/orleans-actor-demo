﻿namespace API.Requests
{
    public class GetFieldResponse
    {
        public string? Value { get; init; } = default!;
    }
}
