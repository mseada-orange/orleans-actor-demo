﻿namespace API.Requests
{
    public class SetFieldResponse
    {
        public bool Success { get; init; } = default!;
    }
}
