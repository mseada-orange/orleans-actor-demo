﻿using Grains;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;

var builder = Host.CreateDefaultBuilder(args);

var siloPort = args.Length > 0 ? int.Parse(args[0]) : 7000;
var gatewayPort = args.Length > 1 ? int.Parse(args[1]) : 9000;
// var serviceId = args.Length > 2 ? args[2] : "Datalake360";

var redisConnectionString = "localhost:6379";

builder.UseOrleans((context, siloBuilder) =>
{
    siloBuilder.UseRedisClustering(options => options.ConnectionString = redisConnectionString);
    siloBuilder.Configure<ClusterOptions>(options =>
    {
        options.ClusterId = "dev";
        options.ServiceId = "Datalake360";
    });

    siloBuilder.AddRedisGrainStorage("dialStore", options => options.ConnectionString = redisConnectionString);

    siloBuilder.ConfigureEndpoints(siloPort, gatewayPort, System.Net.Sockets.AddressFamily.InterNetwork, true);
    siloBuilder.ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(GrainsAssemblyMarker).Assembly).WithReferences());
    siloBuilder.ConfigureLogging(logging => logging.AddConsole());

});

await builder.RunConsoleAsync();